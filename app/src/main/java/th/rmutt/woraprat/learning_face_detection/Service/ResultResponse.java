package th.rmutt.woraprat.learning_face_detection.Service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public class ResultResponse {

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("name_like1")
    @Expose
    String nameLike1;

    @SerializedName("name_like2")
    @Expose
    String nameLike2;

    @SerializedName("percentage1")
    @Expose
    double percentage1;

    @SerializedName("percentage2")
    @Expose
    double percentage2;

    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("allname")
    @Expose
    List<String> allname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public List<String> getAllname() {
        return allname;
    }

    public void setAllname(List<String> allname) {
        this.allname = allname;
    }

    public String getNameLike1() {
        return nameLike1;
    }

    public void setNameLike1(String nameLike1) {
        this.nameLike1 = nameLike1;
    }

    public String getNameLike2() {
        return nameLike2;
    }

    public void setNameLike2(String nameLike2) {
        this.nameLike2 = nameLike2;
    }

    public double getPercentage1() {
        percentage1 = percentage1 * 100;
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        percentage1 = Double.parseDouble(decimalFormat.format(percentage1));
        return percentage1;
    }

    public void setPercentage1(double percentage1) {
        this.percentage1 = percentage1;
    }

    public double getPercentage2() {
        percentage2 = percentage2 * 100;
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        percentage2 = Double.parseDouble(decimalFormat.format(percentage2));
        return percentage2;
    }

    public void setPercentage2(double percentage2) {
        this.percentage2 = percentage2;
    }
}
