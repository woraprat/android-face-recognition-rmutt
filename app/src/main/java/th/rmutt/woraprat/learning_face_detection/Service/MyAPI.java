package th.rmutt.woraprat.learning_face_detection.Service;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface MyAPI {

    @Multipart
    @POST("/face-recognition/recogface")
    Call<ResultResponse> uploadFile(@Part MultipartBody.Part image);

    @Multipart
    @POST("/face-recognition/newface")
    Call<ResultResponse> uploadDataset(@Part MultipartBody.Part image, @Part MultipartBody.Part name);

    @GET("/")
    Call<ResultResponse> allName();

}
