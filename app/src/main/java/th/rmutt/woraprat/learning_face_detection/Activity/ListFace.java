package th.rmutt.woraprat.learning_face_detection.Activity;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import th.rmutt.woraprat.learning_face_detection.MainActivity;
import th.rmutt.woraprat.learning_face_detection.R;
import th.rmutt.woraprat.learning_face_detection.Service.MyAPI;
import th.rmutt.woraprat.learning_face_detection.Service.ResultResponse;
import th.rmutt.woraprat.learning_face_detection.adapter.ListNameAdapter;


public class ListFace extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_face);

        final ListView listView = (ListView) findViewById(R.id.lst_name);

        getSupportActionBar().setTitle("รายชื่อทั้งหมด");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        MyAPI service = new Retrofit.Builder()
                .baseUrl(MainActivity.UPLOAD_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(MyAPI.class);

        Call<ResultResponse> call = service.allName();
        call.enqueue(new Callback<ResultResponse>() {
            @Override
            public void onResponse(Call<ResultResponse> call, Response<ResultResponse> response) {
                if (response.isSuccessful()){
                    Log.d("Successul", "result: " + response.body().getAllname());
                    listView.setAdapter(new ListNameAdapter(ListFace.this, R.layout.item_list_name, response.body().getAllname()));
                }
            }

            @Override
            public void onFailure(Call<ResultResponse> call, Throwable t) {
                Log.d("Failure", "Failure");
            }
        });
    }
}
