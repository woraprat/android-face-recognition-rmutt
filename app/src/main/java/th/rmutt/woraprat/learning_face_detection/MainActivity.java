package th.rmutt.woraprat.learning_face_detection;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import th.rmutt.woraprat.learning_face_detection.Activity.AddDataSet;
import th.rmutt.woraprat.learning_face_detection.Activity.FaceRecognition;
import th.rmutt.woraprat.learning_face_detection.Activity.ListFace;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CAMERA_PERMISSION = 1;
    public static final int REQUEST_WRITE_PERMISSION  = 20;
    public static final String UPLOAD_HOST = "http://35.197.148.12:80";
//    public static final String UPLOAD_HOST = "http://192.168.1.41:5000";
    private Button buttonFaceRecog;
    private Button buttonAddFace;
    private Button buttonListFace;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();
        init();

        checkCameraPermission();
        MyTTS.getInstance(getApplicationContext()).speak("เมนูหลัก");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyTTS.getInstance(getApplicationContext()).speak("เมนูหลัก");
    }

    private void init() {
        mContext = this;
        buttonFaceRecog = (Button) findViewById(R.id.face_recognition);
        buttonAddFace = (Button) findViewById(R.id.add_data_set);
        buttonListFace = (Button) findViewById(R.id.list_face);

        onClick();
    }

    private void onClick() {
        buttonFaceRecog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "เปิดกล้องหลัง", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this, FaceRecognition.class);
                startActivity(intent);
            }
        });

        buttonAddFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddDataSet.class);
                startActivity(intent);
            }
        });

        buttonListFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "List รายชื่อ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, ListFace.class);
                startActivity(intent);
            }
        });
    }

    protected boolean checkCameraPermission() {

        boolean granted = (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

        if(granted) {

            return true;

        } else {

            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                buildPermissionDialog();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
            }
        }

        return false;
    }

    protected void buildPermissionDialog() {

        ConfirmationDialogFragment
                .newInstance(R.string.camera_permission_confirmation,
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_PERMISSION,
                        R.string.camera_permission_not_granted)
                .show(getSupportFragmentManager(), "dialog");
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static ConfirmationDialogFragment newInstance(@StringRes int message,
                                                             String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }
    }
}
