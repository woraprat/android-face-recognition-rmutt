package th.rmutt.woraprat.learning_face_detection.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import th.rmutt.woraprat.learning_face_detection.R;

public class ListNameAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> names;

    public ListNameAdapter(@NonNull Context context, int resource,@NonNull List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.names = objects;
    }

    @NonNull
    @Override
    public View getView(int position,@Nullable View convertView,@NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_list_name, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.txtName);

        textView.setText(names.get(position));

        return rowView;
    }
}
